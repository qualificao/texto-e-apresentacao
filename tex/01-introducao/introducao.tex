%%
%% Capítulo 1: Modelo de Capítulo
%%

% Está sendo usando o comando \mychapter, que foi definido no arquivo
% comandos.tex. Este comando \mychapter é essencialmente o mesmo que o
% comando \chapter, com a diferença que acrescenta um \thispagestyle{empty}
% após o \chapter. Isto é necessário para corrigir um erro de LaTeX, que
% coloca um número de página no rodapé de todas as páginas iniciais dos
% capítulos, mesmo quando o estilo de numeração escolhido é outro.
\mychapter{Introdução}
\label{Cap:introducao}

Com o aumento da complexidade das operações realizadas na indústria e a sobrecarga de atividades dos trabalhadores, podemos identificar ações que poderiam ser desempenhadas por um sistema autônomo abastecido por uma base de conhecimento. Deste modo, este trabalho objetiva a criação de um sistema para auxílio à tomada de decisão no formato de um sistema computacional baseado em regras.

A abordagem adotada pretende desenvolver uma metodologia para criação\-/e\-di\-ção\-/re\-mo\-ção de regras aplicadas a processos industriais com capacidade de auto-aprendizagem. Sistemas especialistas (SE)\nomenclature{SE}{Sistemas Especialistas}, como também são conhecidos, representa uma classe importante de sistemas inteligentes que tratam um conjunto de ações executadas quando determinadas condições são satisfeitas. SE procuram reproduzir as experiências dos operadores do sistema quando determinadas situações ocorrem. Assume-se então que os especialistas tomem atitudes a partir de determinadas situações e estas instruções podem ser incorporadas ao sistema com o intuito de reduzir o esforço humano físico ou intelectual. Sumariamente, os especialistas são compostos de:

\begin{itemize}
	\item Uma coleção de conhecimentos já definidos. Incluindo o domínio reduzido em que as atividades podem ser realizadas no que se refere as ações e condições disponíveis;
	\item Escolhas feitas por humanos e armazenados de maneira a ser compreendido por máquinas que reproduzem essas tomadas de decisões.
\end{itemize}

Duas dificuldades são apresentadas na criação de um sistema especialista e serão abordadas neste trabalho:

\begin{itemize}
	\item Dificuldade em extrair um conjunto inicial de regras, que será tratado a partir de entrevista com um operador identificando suas principais atividades realizadas;
	\item Dificuldade em atualizar as regras (auto-aprendizagem), Que será contornado utilizando algoritmos de remoção, atualização e criação de regras.
\end{itemize}

O trabalho também prevê a criação, melhoramento e remoção de regras a partir de diagnósticos realizados durante as atividades dos operadores e também de regras que não estão sendo utilizadas e podem ser candidatas a serem extintas.

\section{Motivação}

No ambiente industrial, os operadores estão a todo momento tomando decisões importantes que devem ser determinadas o mais rápido possível. A ação correta ou mesmo a intervenção do operador em situações extremas protege não somente a segurança dos trabalhadores e  a preservação dos equipamentos, como também, prioriza o aumento da produção e a redução de custos.

Deixar a encargo do operador essas decisões pode não ser uma boa alternativa. Devido aos erros humanos provocados por estresse, falta de atenção, fadiga, por exemplo, uma ação pode não ser a apropriada ou não vir a ser tomada para que o sistema funcione como o planejado.

Para evitar esse problema, Sistemas Especialistas possuem três qualidades que viabilizam seu uso:

\begin{itemize}
	\item Conseguem gerenciar um maior número de problemas que podem ocorrer;
	\item Conseguem avaliar quase que instantaneamente a situação antes de avaliar a atitude a ser tomada se comparada com as ações do operador;
	\item Se implementado de maneira correta, com boa avaliação e interpretação do sistema que está operando, agirá sempre com boas ações.
\end{itemize}

Além desses fatores, o sistema especialista possui outras vantagens que são inerentes ao melhoramento dos processos industriais:

\begin{itemize}
	\item Minimização do custo de manutenção dos equipamentos. Já que o sistema sempre opera com a melhor ação que deve ser tomada;
	\item Redução da carga de trabalho dos operadores. Os operadores poderão se dedicar a outras tarefas que exigem um esforço intelectual maior;
	\item Aumento da produtividade advindo da execução correta das ações;
	\item Facilidade do rastreio das ações tomadas. Todas as ações são tomadas devido ao fato de que certas situações ocorreram e o sistema computacional pode registrar os eventos que ocasionaram a ação;
	\item Possibilidade de armazenamento das frequências das regras ativadas. O sistema pode melhorar a medida que verificamos se o disparo de determinadas regras são mais ou menos frequentes.
\end{itemize}
\section{Objetivos}
O sistema pretendido possui os seguintes objetivos gerais:
\begin{itemize}
	\item Apresentar o modelo de um sistema que auxilia a tomada de decisão dos operadores baseado em regras;
	\item Aplicar a tecnologia desenvolvida em um sistema de automação industrial;
	\item Reduzir a dependência dos operadores em tomadas de decisão;
	\item Incrementar o sistema com base na adição de novas regras advindas diretamente pela intervenção dos operadores;
	\item Incrementar o sistema com base na criação de regras baseados no histórico das ações tomadas na planta;
	\item Sugerir modificações nas regras avaliando a contribuição de cada regra ao sistema;
	\item Testar o sistema em um ambiente mais próximo possível de um sistema real.
\end{itemize}

São objetivos secundários pretendidos ao alcançar as atividades planejadas:
\begin{itemize}
	\item Criação de motor de inferência baseado no esquema \emph{forward} para tomada de ações;
	\item Criação de motor de inferência baseado no esquema \emph{backward} com o intuito de diagnóstico;
	\item Resolver conflitos de regras apresentando uma solução ao operador;
	\item Armazenar todas as ações tomadas pelo sistema especialista para aperfeiçoar as ações;
	\item Definição de rotina para inicialização das regras através de entrevista com os operadores.
\end{itemize}

A dificuldade em implementar um sistema especialista consiste em construir regras consistentes que realmente apresentem um ganho no tempo do operador. Pensando nisso, o trabalho aborda a implementação de um sistema de auto-aprendizado baseado em regras que seja autônomas e dinâmicas o suficiente para reduzir a carga de trabalho dos operadores objetivando redução de custos, a segurança e com a manutenção da produtividade.

\section{Organização do texto}
Além deste capítulo, que apresenta características gerais sobre sistemas especialistas em geral e o sistema proposto, o capítulo~\ref{Cap:FundamentacaoTeorica} apresenta a classificação de sistemas baseado em regras e sua localização numa área maior de sistemas inteligentes, juntamente com as definições e diagramas que explicam os componentes desses sistemas. Na parte final do capítulo, são apresentadas as definições importantes e gerais sobre sistemas com representação do conhecimento na abordagem Conexionista e Simbólica. Apresentando também modelos híbridos que unem qualidades das duas abordagens. No capítulo 3, um histórico de trabalhos são apresentados sobre sistemas especialistas na área industrial e outras áreas. No capítulo 4 a proposta de trabalho é apresentada e no último capítulo é apresentada a conclusão e cronograma de atividades.

